package fr.keke142.redisplayerdatabridge.configs;

import org.bukkit.configuration.Configuration;

public class SyncConfig {
    private int saveIntervalSecs;
    private int maxDataAgeSecs;

    public void load(Configuration config) {
        saveIntervalSecs = config.getInt("sync.saveIntervalSecs", 60);
        maxDataAgeSecs = config.getInt("sync.maxDataAgeSecs", 300);
    }

    public void save(Configuration config) {
        config.set("sync.saveIntervalSecs", saveIntervalSecs);
        config.set("sync.maxDataAgeSecs", maxDataAgeSecs);
    }

    public int getSaveIntervalSecs() {
        return saveIntervalSecs;
    }

    public int getMaxDataAgeSecs() {
        return maxDataAgeSecs;
    }
}
