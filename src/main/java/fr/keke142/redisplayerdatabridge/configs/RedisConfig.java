package fr.keke142.redisplayerdatabridge.configs;

import org.bukkit.configuration.Configuration;

public class RedisConfig {
    private String host;
    private int port;
    private String password;

    public void load(Configuration config) {
        host = config.getString("database.host", "localhost");
        port = config.getInt("database.port", 6379);
        password = config.getString("database.password", "");
    }

    public void save(Configuration config) {
        config.set("database.host", host);
        config.set("database.port", port);
        config.set("database.password", password);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getPassword() {
        return password;
    }
}
