package fr.keke142.redisplayerdatabridge.managers;

import fr.keke142.redisplayerdatabridge.RedisPlayerDataBridgePlugin;
import fr.keke142.redisplayerdatabridge.objects.PlayerData;
import fr.keke142.redisplayerdatabridge.utils.EffectsSerializerUtil;
import fr.keke142.redisplayerdatabridge.utils.ExperienceUtil;
import fr.keke142.redisplayerdatabridge.utils.InventorySerializerUtil;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.redisson.api.RMap;

import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;

public class PlayerDataManager {

    private final RMap<String, PlayerData> playersData;
    private final RedisPlayerDataBridgePlugin plugin;

    public PlayerDataManager(RedisPlayerDataBridgePlugin plugin) {
        this.plugin = plugin;

        this.playersData = plugin.getRedisManager().getPlayersData();
    }

    public void loadPlayerData(Player p) {
        String uuid = p.getUniqueId().toString();

        if (!playersData.containsKey(uuid)) return;

        PlayerData playerData = playersData.get(uuid);

        double health = playerData.getHealth();
        int foodLevel = playerData.getFoodLevel();
        double saturationLevel = playerData.getSaturationLevel();
        String gamemode = playerData.getGamemode();

        String serializedInventory = playerData.getSerializedInventory();
        String serializedArmor = playerData.getSerializedArmor();

        int heldSlot = playerData.getHeldSlot();

        String serializedEnderchest = playerData.getSerializedEnderchest();

        int experience = playerData.getExperienceLevel();

        String serializedPotionsEffects = playerData.getSerializedPotionEffects();

        p.setHealth(health);
        p.setFoodLevel(foodLevel);
        p.setSaturation((float) saturationLevel);
        p.setGameMode(GameMode.valueOf(gamemode));

        ExperienceUtil.setTotalExperience(p, experience);
        try {
            ItemStack[] inventory = InventorySerializerUtil.itemStackArrayFromBase64(serializedInventory);
            ItemStack[] inventoryArmor = InventorySerializerUtil.itemStackArrayFromBase64(serializedArmor);
            ItemStack[] enderchest = InventorySerializerUtil.itemStackArrayFromBase64(serializedEnderchest);
            Collection<PotionEffect> potionEffects = EffectsSerializerUtil.potionEffectsFromBase64(serializedPotionsEffects);

            p.getInventory().setContents(inventory);
            p.getInventory().setArmorContents(inventoryArmor);

            p.getInventory().setHeldItemSlot(heldSlot);

            p.getEnderChest().setContents(enderchest);

            for (PotionEffect effect : p.getActivePotionEffects()) {
                p.removePotionEffect(effect.getType());
            }

            if (potionEffects != null) p.addPotionEffects(potionEffects);
        } catch (IOException e) {
            plugin.getLogger().log(Level.SEVERE, "Failed to load data of " + p.getName(), e);
            p.kickPlayer(MessageManager.msg("failedToLoadData"));
        }

    }

    public void savePlayerData(Player p) {
        double health = p.getHealth();
        int foodLevel = p.getFoodLevel();
        double saturationLevel = p.getSaturation();
        String gamemode = p.getGameMode().name();

        String[] serializedInventories = InventorySerializerUtil.playerInventoryToBase64(p.getInventory());
        String serializedInventory = serializedInventories[0];
        String serializedArmor = serializedInventories[1];
        int heldSlot = p.getInventory().getHeldItemSlot();

        String enderchest = InventorySerializerUtil.toBase64(p.getEnderChest());

        int experience = ExperienceUtil.getTotalExperience(p);

        String potionEffects = EffectsSerializerUtil.potionEffectsToBase64(p.getActivePotionEffects());

        PlayerData playerData = new PlayerData(health, foodLevel, saturationLevel, gamemode, serializedInventory, serializedArmor, heldSlot, enderchest, experience, potionEffects);

        playersData.put(p.getUniqueId().toString(), playerData);
    }

    public void cleanOldPlayersData() {
        playersData.forEach((uuid, playerData) -> {
            long currentTime = System.currentTimeMillis();
            long updateTime = playerData.getUpdateTime();

            long timeDiff = currentTime - updateTime;
            int maxAgeMs = plugin.getConfigManager().getSyncConfig().getMaxDataAgeSecs() * 1000;

            if (timeDiff > maxAgeMs) {
                playersData.remove(uuid);
            }
        });
    }

    public void saveAllPlayersData() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            savePlayerData(p);
        }
    }
}
