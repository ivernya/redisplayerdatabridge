package fr.keke142.redisplayerdatabridge.managers;

import fr.keke142.redisplayerdatabridge.RedisPlayerDataBridgePlugin;
import fr.keke142.redisplayerdatabridge.configs.RedisConfig;
import fr.keke142.redisplayerdatabridge.objects.PlayerData;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;

public class RedisManager {
    private final RedisPlayerDataBridgePlugin plugin;
    private RMap<String, PlayerData> playersData;
    private RedissonClient redissonClient;

    public RedisManager(RedisPlayerDataBridgePlugin plugin) {
        this.plugin = plugin;
    }

    public void load() {
        RedisConfig redisConfig = plugin.getConfigManager().getRedisConfig();

        Config redissonConfig = new Config();
        if (redisConfig.getPassword() == null || redisConfig.getPassword().isEmpty()) {
            redissonConfig.useSingleServer().setAddress("redis://" + redisConfig.getHost() + ":" + redisConfig.getPort());
        } else {
            redissonConfig.useSingleServer().setAddress("redis://" + redisConfig.getHost() + ":" + redisConfig.getPort()).setPassword(redisConfig.getPassword());
        }

        redissonConfig.setCodec(new JsonJacksonCodec());

        redissonClient = Redisson.create(redissonConfig);

        playersData = redissonClient.getMap("playersData");
    }

    public RedissonClient getRedissonClient() {
        return redissonClient;
    }

    public RMap<String, PlayerData> getPlayersData() {
        return playersData;
    }
}
