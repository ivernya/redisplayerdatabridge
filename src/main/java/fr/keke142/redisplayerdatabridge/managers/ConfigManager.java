package fr.keke142.redisplayerdatabridge.managers;

import fr.keke142.redisplayerdatabridge.RedisPlayerDataBridgePlugin;
import fr.keke142.redisplayerdatabridge.configs.RedisConfig;
import fr.keke142.redisplayerdatabridge.configs.SyncConfig;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {
    private final RedisPlayerDataBridgePlugin plugin;
    private final RedisConfig redisConfig = new RedisConfig();
    private final SyncConfig syncConfig = new SyncConfig();

    private final File file;
    private FileConfiguration config;

    public ConfigManager(RedisPlayerDataBridgePlugin plugin) {
        this.plugin = plugin;
        file = new File(plugin.getDataFolder(), "config.yml");
        plugin.createDefaultConfiguration(file, "config.yml");

        config = plugin.getConfig();
        config.options().copyDefaults(true);
        plugin.saveDefaultConfig();

        redisConfig.load(config);
        syncConfig.load(config);
    }

    public void save() {
        try {
            redisConfig.save(config);
            syncConfig.save(config);

            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reload() {
        config = plugin.getConfig();
        config.options().copyDefaults(true);
        plugin.saveDefaultConfig();

        redisConfig.load(config);
        syncConfig.load(config);
    }

    public RedisConfig getRedisConfig() {
        return redisConfig;
    }

    public SyncConfig getSyncConfig() {
        return syncConfig;
    }
}
