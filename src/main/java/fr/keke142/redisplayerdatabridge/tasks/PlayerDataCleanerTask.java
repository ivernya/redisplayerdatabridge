package fr.keke142.redisplayerdatabridge.tasks;

import fr.keke142.redisplayerdatabridge.RedisPlayerDataBridgePlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerDataCleanerTask extends BukkitRunnable {
    private RedisPlayerDataBridgePlugin plugin;

    public PlayerDataCleanerTask(RedisPlayerDataBridgePlugin plugin) {
        runTaskTimerAsynchronously(plugin, 20, 20);

        this.plugin = plugin;
    }

    @Override
    public void run() {
        plugin.getPlayerDataManager().cleanOldPlayersData();
    }
}
