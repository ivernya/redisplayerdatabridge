package fr.keke142.redisplayerdatabridge.tasks;

import fr.keke142.redisplayerdatabridge.RedisPlayerDataBridgePlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerDataAutoSaveTask extends BukkitRunnable {
    private RedisPlayerDataBridgePlugin plugin;

    public PlayerDataAutoSaveTask(RedisPlayerDataBridgePlugin plugin) {
        int saveIntervalSecs = plugin.getConfigManager().getSyncConfig().getSaveIntervalSecs() * 20;

        runTaskTimerAsynchronously(plugin, saveIntervalSecs, saveIntervalSecs);

        this.plugin = plugin;
    }

    @Override
    public void run() {
        plugin.getPlayerDataManager().saveAllPlayersData();
        plugin.getLogger().info("Automatically saved all players data in cache");
    }
}
