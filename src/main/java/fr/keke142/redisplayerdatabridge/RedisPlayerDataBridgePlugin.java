package fr.keke142.redisplayerdatabridge;

import fr.keke142.redisplayerdatabridge.listeners.PlayerDataRestoreSubscriber;
import fr.keke142.redisplayerdatabridge.listeners.PlayerQuitListener;
import fr.keke142.redisplayerdatabridge.managers.ConfigManager;
import fr.keke142.redisplayerdatabridge.managers.MessageManager;
import fr.keke142.redisplayerdatabridge.managers.PlayerDataManager;
import fr.keke142.redisplayerdatabridge.managers.RedisManager;
import fr.keke142.redisplayerdatabridge.tasks.PlayerDataAutoSaveTask;
import fr.keke142.redisplayerdatabridge.tasks.PlayerDataCleanerTask;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.Locale;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class RedisPlayerDataBridgePlugin extends JavaPlugin {
    private ConfigManager configManager;
    private RedisManager redisManager;
    private PlayerDataManager playerDataManager;

    @Override
    public void onEnable() {
        configManager = new ConfigManager(this);
        redisManager = new RedisManager(this);
        redisManager.load();

        MessageManager.loadLocale(this, Locale.FRENCH);

        playerDataManager = new PlayerDataManager(this);

        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new PlayerQuitListener(this), this);
        pluginManager.registerEvents(new PlayerDataRestoreSubscriber(this), this);

        new PlayerDataAutoSaveTask(this);
        new PlayerDataCleanerTask(this);
    }

    @Override
    public void onDisable() {
        playerDataManager.saveAllPlayersData();

        redisManager.getRedissonClient().shutdown();
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public RedisManager getRedisManager() {
        return redisManager;
    }

    public PlayerDataManager getPlayerDataManager() {
        return playerDataManager;
    }

    /**
     * Create a default configuration file from the .jar.
     *
     * @param actual      The destination file
     * @param defaultName The name of the file inside the jar's defaults folder
     */
    public void createDefaultConfiguration(File actual, String defaultName) {

        // Make parent directories
        File parent = actual.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }

        if (actual.exists()) {
            return;
        }

        JarFile file = null;
        InputStream input = null;
        try {
            file = new JarFile(getFile());
            ZipEntry copy = file.getEntry(defaultName);
            if (copy == null) {
                file.close();
                throw new FileNotFoundException();
            }
            input = file.getInputStream(copy);
        } catch (IOException e) {
            getLogger().severe("Unable to read default configuration: " + defaultName);
        }

        if (input != null) {
            FileOutputStream output = null;

            try {
                output = new FileOutputStream(actual);
                byte[] buf = new byte[8192];
                int length = 0;
                while ((length = input.read(buf)) > 0) {
                    output.write(buf, 0, length);
                }

                getLogger().info("Default configuration file written: " + actual.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    input.close();
                } catch (IOException ignore) {
                }

                try {
                    if (output != null) {
                        output.close();
                    }
                } catch (IOException ignore) {
                }
            }
        }
        if (file != null) {
            try {
                file.close();
            } catch (IOException ignore) {
            }
        }
    }
}
