package fr.keke142.redisplayerdatabridge.listeners;

import fr.keke142.redisplayerdatabridge.RedisPlayerDataBridgePlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {
    private RedisPlayerDataBridgePlugin plugin;

    public PlayerQuitListener(RedisPlayerDataBridgePlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        plugin.getPlayerDataManager().savePlayerData(player);
        plugin.getRedisManager().getRedissonClient().getTopic("syncPlayer").publish(player.getUniqueId().toString());
    }
}
