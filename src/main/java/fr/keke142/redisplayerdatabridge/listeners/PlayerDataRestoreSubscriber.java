package fr.keke142.redisplayerdatabridge.listeners;

import fr.keke142.redisplayerdatabridge.RedisPlayerDataBridgePlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.redisson.api.RTopic;

import java.util.UUID;

public class PlayerDataRestoreSubscriber implements Listener {
    private static final int MAX_PLAYERDATA_WAITING_MS = 5;

    public PlayerDataRestoreSubscriber(RedisPlayerDataBridgePlugin plugin) {
        RTopic topic = plugin.getRedisManager().getRedissonClient().getTopic("syncPlayer");
        topic.addListener(String.class, (charSequence, playerUuid) -> Bukkit.getScheduler().runTask(plugin, () -> {
            long startTime = System.currentTimeMillis();

            while ((System.currentTimeMillis() - startTime) < MAX_PLAYERDATA_WAITING_MS) {
                Player p = Bukkit.getPlayer(UUID.fromString(playerUuid));

                if (p != null) {
                    plugin.getPlayerDataManager().loadPlayerData(p);
                    break;
                }
            }
        }));
    }
}
