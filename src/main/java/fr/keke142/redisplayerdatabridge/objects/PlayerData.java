package fr.keke142.redisplayerdatabridge.objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PlayerData implements Serializable {
    @JsonProperty("health")
    private final double health;

    @JsonProperty("foodLevel")
    private final int foodLevel;

    @JsonProperty("saturationLevel")
    private final double saturationLevel;

    @JsonProperty("gamemode")
    private final String gamemode;

    @JsonProperty("serializedInventory")
    private final String serializedInventory;

    @JsonProperty("serializedArmor")
    private final String serializedArmor;

    @JsonProperty("heldSlot")
    private final int heldSlot;

    @JsonProperty("serializedEnderchest")
    private final String serializedEnderchest;

    @JsonProperty("experienceLevel")
    private final int experienceLevel;

    @JsonProperty("serializedPotionEffects")
    private final String serializedPotionEffects;

    @JsonProperty("updateTime")
    private final long updateTime;

    @JsonCreator
    public PlayerData(@JsonProperty("health") double health, @JsonProperty("foodLevel") int foodLevel, @JsonProperty("saturationLevel") double saturationLevel, @JsonProperty("gamemode") String gamemode, @JsonProperty("serializedInventory") String serializedInventory, @JsonProperty("serializedArmor") String serializedArmor, @JsonProperty("heldSlot") int heldSlot, @JsonProperty("serializedEnderchest") String serializedEnderchest, @JsonProperty("experienceLevel") int experienceLevel, @JsonProperty("serializedPotionEffects") String serializedPotionEffects) {
        this.health = health;
        this.foodLevel = foodLevel;
        this.saturationLevel = saturationLevel;
        this.gamemode = gamemode;
        this.serializedInventory = serializedInventory;
        this.serializedArmor = serializedArmor;
        this.heldSlot = heldSlot;
        this.serializedEnderchest = serializedEnderchest;
        this.experienceLevel = experienceLevel;
        this.serializedPotionEffects = serializedPotionEffects;
        this.updateTime = System.currentTimeMillis();
    }

    @JsonGetter("health")
    public double getHealth() {
        return health;
    }

    @JsonGetter("foodLevel")
    public int getFoodLevel() {
        return foodLevel;
    }

    @JsonGetter("saturationLevel")
    public double getSaturationLevel() {
        return saturationLevel;
    }

    @JsonGetter("gamemode")
    public String getGamemode() {
        return gamemode;
    }

    @JsonGetter("serializedInventory")
    public String getSerializedInventory() {
        return serializedInventory;
    }

    @JsonGetter("serializedArmor")
    public String getSerializedArmor() {
        return serializedArmor;
    }

    @JsonGetter("heldSlot")
    public int getHeldSlot() {
        return heldSlot;
    }

    @JsonGetter("serializedEnderchest")
    public String getSerializedEnderchest() {
        return serializedEnderchest;
    }

    @JsonGetter("experienceLevel")
    public int getExperienceLevel() {
        return experienceLevel;
    }

    @JsonGetter("serializedPotionEffects")
    public String getSerializedPotionEffects() {
        return serializedPotionEffects;
    }

    @JsonGetter("updateTime")
    public long getUpdateTime() {
        return updateTime;
    }
}
