package fr.keke142.redisplayerdatabridge.utils;

import org.bukkit.potion.PotionEffect;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class EffectsSerializerUtil {
    private EffectsSerializerUtil() {
        throw new UnsupportedOperationException();
    }

    public static String potionEffectsToBase64(Collection<PotionEffect> paramCollection) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream bukkitObjectOutputStream = new BukkitObjectOutputStream(byteArrayOutputStream);
            bukkitObjectOutputStream.writeInt((paramCollection.toArray()).length);
            for (byte b = 0; b < (paramCollection.toArray()).length; b++)
                bukkitObjectOutputStream.writeObject(paramCollection.toArray()[b]);
            bukkitObjectOutputStream.close();
            return Base64Coder.encodeLines(byteArrayOutputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save potion effect.", e);
        }
    }

    public static Collection<PotionEffect> potionEffectsFromBase64(String paramString) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(Base64Coder.decodeLines(paramString));
            BukkitObjectInputStream bukkitObjectInputStream = new BukkitObjectInputStream(byteArrayInputStream);
            PotionEffect[] arrayOfPotionEffect = new PotionEffect[bukkitObjectInputStream.readInt()];
            List<PotionEffect> arrayList = new ArrayList<>();

            for (byte b = 0; b < arrayOfPotionEffect.length; b++)
                arrayList.add((PotionEffect) bukkitObjectInputStream.readObject());

            bukkitObjectInputStream.close();
            return arrayList;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}